<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/9/2018
 * Time: 11:58 AM
 */

use OGC\SLD\NamedLayer;
use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\FeatureTypeStyle;
use OGC\SLD\SE\LineSymbolizer;
use OGC\SLD\SE\Rule;
use OGC\SLD\SE\RuleDescription;
use OGC\SLD\SE\Stroke;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\StyledLayerDescriptor;
use OGC\SLD\UserStyle;
use PHPUnit\Framework\TestCase;

class SLD_StyledLayerDescriptorTest extends TestCase
{

    const STANDARD_XML_PRETTIFIED =
        "<StyledLayerDescriptor version=\"1.0.0\" ".
            "xsi:schemaLocation=\"http://www.opengis.net/sld StyledLayerDescriptor.xsd\" ".
            "xmlns=\"http://www.opengis.net/sld\" ".
            "xmlns:ogc=\"http://www.opengis.net/ogc\" ".
            "xmlns:xlink=\"http://www.w3.org/1999/xlink\" ".
            "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n".
        "</StyledLayerDescriptor>";

    const FULL_XML_PRETTIFIED =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
        "<StyledLayerDescriptor version=\"1.0.0\" ".
        "xsi:schemaLocation=\"http://www.opengis.net/sld StyledLayerDescriptor.xsd\" ".
        "xmlns=\"http://www.opengis.net/sld\" ".
        "xmlns:ogc=\"http://www.opengis.net/ogc\" ".
        "xmlns:xlink=\"http://www.w3.org/1999/xlink\" ".
        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n".
        "\t<NamedLayer>\n".
        "\t\t<Name>default_line</Name>\n".
        "\t\t<UserStyle>\n".
        "\t\t\t<Title>Default Line</Title>\n".
        "\t\t\t<Abstract>A sample style that draws a line</Abstract>\n".
        "\t\t\t<FeatureTypeStyle>\n".
        "\t\t\t\t<Rule>\n".
        "\t\t\t\t\t<Name>rule1</Name>\n".
        "\t\t\t\t\t<Title>Blue Line</Title>\n".
        "\t\t\t\t\t<Abstract>A solid blue line with a 1 pixel width</Abstract>\n".
        "\t\t\t\t\t<LineSymbolizer>\n".
        "\t\t\t\t\t\t<Stroke>\n".
        "\t\t\t\t\t\t\t<CssParameter name=\"stroke\">#0000FF</CssParameter>\n".
        "\t\t\t\t\t\t</Stroke>\n".
        "\t\t\t\t\t</LineSymbolizer>\n".
        "\t\t\t\t</Rule>\n".
        "\t\t\t</FeatureTypeStyle>\n".
        "\t\t</UserStyle>\n".
        "\t</NamedLayer>\n".
        "</StyledLayerDescriptor>";

    /**
     * @test
     */
    public function testCanOutputStandardXMLPrettified(){

        $sld = new StyledLayerDescriptor();
        $this->assertSame(self::STANDARD_XML_PRETTIFIED, $sld->applyNamespace(false)->__toString());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutpuFullXMLPrettified(){

        //Create stroke
        $cssParameter = CssParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF')->applyNamespace(false);
        $stroke = Stroke::fromStyles($cssParameter)->applyNamespace(false);

        //Create line symbolizer
        $lineSymbolizer = new LineSymbolizer(null, $stroke);
        $lineSymbolizer->applyNamespace(false);

        //Create version 1.0 type description for rule
        $description = new RuleDescription('Blue Line', 'A solid blue line with a 1 pixel width');
        $rule = new Rule('rule1', $description->usePlain(true)->applyNamespace(false));
        $rule->addSymbolizers($lineSymbolizer)->applyNamespace(false);

        //Create feature style
        $featureStyle = new FeatureTypeStyle();
        $featureStyle->addRules($rule)->applyNamespace(false);

        //Create version 1.0 type of description for user style
        $description_userStyle = new RuleDescription('Default Line', 'A sample style that draws a line');
        $description_userStyle->applyNamespace(false);

        //Create user style
        $userStyle = new UserStyle(null, $description_userStyle->usePlain(true));
        $userStyle->addFeatureTypeStyles($featureStyle)->applyNamespace(false);

        //Create named layer
        $namedLayer = new NamedLayer('default_line');
        $namedLayer->addStyles($userStyle)->applyNamespace(false);

        //Create SLD
        $sld = new StyledLayerDescriptor($namedLayer);
        $sld->includeXmlVersion()->applyNamespace(false);

        //Assert
        $this->assertSame(self::FULL_XML_PRETTIFIED, $sld->__toString());

    }


}
