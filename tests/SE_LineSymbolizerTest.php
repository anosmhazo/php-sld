<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/4/2018
 * Time: 9:52 AM
 */

use OGC\SLD\SE\LineSymbolizer;
use OGC\SLD\SE\Stroke;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\SvgParameter;
use PHPUnit\Framework\TestCase;

class SE_LineSymbolizerTest extends TestCase
{

    const TEST_STANDARD_XML = "<LineSymbolizer></LineSymbolizer>";

    const TEST_LINE_SYMBOLIZER_WITH_STROKE_XML_PRETTIFIED =
        "<LineSymbolizer>\n".
        "\t<Stroke>\n".
        "\t\t<SvgParameter name=\"stroke\">#0000FF</SvgParameter>\n".
        "\t\t<SvgParameter name=\"stroke-width\">3</SvgParameter>\n".
        "\t\t<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>\n".
        "\t</Stroke>\n".
        "</LineSymbolizer>";

    const TEST_LINE_SYMBOLIZER_WITH_STROKE_XML =
        "<LineSymbolizer>".
        "<Stroke>".
        "<SvgParameter name=\"stroke\">#0000FF</SvgParameter>".
        "<SvgParameter name=\"stroke-width\">3</SvgParameter>".
        "<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>".
        "</Stroke>".
        "</LineSymbolizer>";

    const TEST_LINE_SYMBOLIZER_WITH_STROKE_AND_OFFSET_XML_PRETTIFIED =
        "<LineSymbolizer>\n".
        "\t<Stroke>\n".
        "\t\t<SvgParameter name=\"stroke\">#0000FF</SvgParameter>\n".
        "\t\t<SvgParameter name=\"stroke-width\">3</SvgParameter>\n".
        "\t\t<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>\n".
        "\t</Stroke>\n".
        "\t<PerpendicularOffset>5</PerpendicularOffset>\n".
        "</LineSymbolizer>";

    const TEST_LINE_SYMBOLIZER_WITH_STROKE_AND_OFFSET_XML =
        "<LineSymbolizer>".
        "<Stroke>".
        "<SvgParameter name=\"stroke\">#0000FF</SvgParameter>".
        "<SvgParameter name=\"stroke-width\">3</SvgParameter>".
        "<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>".
        "</Stroke>".
        "<PerpendicularOffset>5</PerpendicularOffset>".
        "</LineSymbolizer>";

    const TEST_STROKE_SVG_STYLE_XML =
        "<Stroke>".
        "<SvgParameter name=\"stroke\">#0000FF</SvgParameter>".
        "<SvgParameter name=\"stroke-width\">3</SvgParameter>".
        "<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>".
        "</Stroke>";


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputStandardXML(){

        $parameter = new LineSymbolizer();
        $this->assertSame(self::TEST_STANDARD_XML, $parameter->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputWithStrokeXMLPrettified(){

        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        $stroke = Stroke::fromStyles($style1, $style2, $style3);
        $symbolizer = new LineSymbolizer(null, $stroke);

        $this->assertSame(self::TEST_LINE_SYMBOLIZER_WITH_STROKE_XML_PRETTIFIED, $symbolizer->__toString());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputWithStrokeXML(){

        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        $stroke = Stroke::fromStyles($style1, $style2, $style3);
        $symbolizer = new LineSymbolizer(null, $stroke);

        $this->assertSame(self::TEST_LINE_SYMBOLIZER_WITH_STROKE_XML, $symbolizer->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputWithStrokeAndOffsetXMLPrettified(){

        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        $stroke = Stroke::fromStyles($style1, $style2, $style3);
        $symbolizer = new LineSymbolizer(null, $stroke);
        $symbolizer->setPerpendicularOffset(5);

        $this->assertSame(self::TEST_LINE_SYMBOLIZER_WITH_STROKE_AND_OFFSET_XML_PRETTIFIED, $symbolizer->__toString());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputWithStrokeAndOffsetXML(){

        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        $stroke = Stroke::fromStyles($style1, $style2, $style3);
        $symbolizer = new LineSymbolizer(null, $stroke);
        $symbolizer->setPerpendicularOffset(5);

        $this->assertSame(self::TEST_LINE_SYMBOLIZER_WITH_STROKE_AND_OFFSET_XML, $symbolizer->toXML());

    }


}
