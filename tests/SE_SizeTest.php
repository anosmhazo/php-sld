<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 1:55 PM
 */

use OGC\SLD\SE\Opacity;
use OGC\SLD\SE\Size;
use PHPUnit\Framework\TestCase;

class SE_SizeTest extends TestCase
{

    const TEST_VALUE_XML = "<Size>3</Size>";

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputValueXML(){

        $parameter = Size::fromValue(3);
        $this->assertSame(self::TEST_VALUE_XML, $parameter->toXML());

    }

}
