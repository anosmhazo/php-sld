<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 2:03 PM
 */

use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\FeatureTypeStyle;
use OGC\SLD\SE\LineSymbolizer;
use OGC\SLD\SE\Rule;
use OGC\SLD\SE\Stroke;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\UserStyle;
use PHPUnit\Framework\TestCase;

class SLD_UserStyleTest extends TestCase
{

    const STANDARD_TEST_XML =
    "<UserStyle>".
    "<Name>Default Style</Name>".
    "<FeatureTypeStyle>".
    "<Name>name</Name>".
    "<Rule>".
    "<MaxScaleDenominator>750000</MaxScaleDenominator>".
    "<LineSymbolizer>".
    "<Stroke>".
    "<CssParameter name=\"stroke\">#000000</CssParameter>".
    "<CssParameter name=\"stroke-width\">3</CssParameter>".
    "</Stroke>".
    "</LineSymbolizer>".
    "</Rule>".
    "</FeatureTypeStyle>".
    "</UserStyle>";

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputRuleWithDescriptionFilterSymbolizerXML(){

        //Generate styling
        $style1 = CssParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#000000');
        $style2 = CssParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);

        //Generate stroke
        $stroke = Stroke::fromStyles($style1, $style2);

        //Create symbolizer
        $symbolizer = new LineSymbolizer(null, $stroke);

        //Create rule
        $rule = new Rule();
        $rule->setMaxScaleDenominator(750000);
        $rule->addSymbolizers($symbolizer);

        //Create feature type
        $featuretype = new FeatureTypeStyle('name');
        $featuretype->addRules($rule)->applyNamespace(false);

        //Create user style
        $userStyle = new UserStyle('Default Style');
        $userStyle->addFeatureTypeStyles($featuretype);

        $this->assertSame(self::STANDARD_TEST_XML, $userStyle->applyNamespace(false)->toXML());

    }

}
