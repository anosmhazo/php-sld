<?php
/**
 * Tests for feature type style
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 8:29 AM
 */

use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\FeatureTypeStyle;
use OGC\SLD\SE\LineSymbolizer;
use OGC\SLD\SE\Rule;
use OGC\SLD\SE\Stroke;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\SvgParameter;
use PHPUnit\Framework\TestCase;

class SE_FeatureTypeStyleTest extends TestCase
{

    const STANDARD_XML_PRETTIFIED =
        "<FeatureTypeStyle>\n".
        "</FeatureTypeStyle>";

    const STANDARD_XML = "<FeatureTypeStyle></FeatureTypeStyle>";

    const LINE_SYMBOLIZER_XML_PRETTIFIED = 
        "<FeatureTypeStyle>\n".
        "\t<Rule>\n".
        "\t\t<LineSymbolizer>\n".
        "\t\t\t<Stroke>\n".
        "\t\t\t\t<CssParameter name=\"stroke\">#000000</CssParameter>\n".
        "\t\t\t\t<CssParameter name=\"stroke-width\">3</CssParameter>\n".
        "\t\t\t</Stroke>\n".
        "\t\t</LineSymbolizer>\n".
        "\t</Rule>\n".
        "</FeatureTypeStyle>";
    
    const LINE_SYMBOLIZER_XML =
        "<FeatureTypeStyle>".
        "<Rule>".
        "<LineSymbolizer>".
        "<Stroke>".
        "<CssParameter name=\"stroke\">#000000</CssParameter>".
        "<CssParameter name=\"stroke-width\">3</CssParameter>".
        "</Stroke>".
        "</LineSymbolizer>".
        "</Rule>".
        "</FeatureTypeStyle>";

    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $featuretype = new FeatureTypeStyle();
        $this->assertSame(self::STANDARD_XML, $featuretype->applyNamespace(false)->toXML());

    }

    /**
     * @test
     */
    public function testCanOutputStandardXMLPrettified(){

        $featuretype = new FeatureTypeStyle();
        $this->assertSame(self::STANDARD_XML_PRETTIFIED, $featuretype->applyNamespace(false)->__toString());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputRuleWithDescriptionFilterSymbolizerXMLPrettified(){

        //Generate styling
        $style1 = CssParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#000000');
        $style2 = CssParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);

        //Generate stroke
        $stroke = Stroke::fromStyles($style1, $style2);

        //Create symbolizer
        $symbolizer = new LineSymbolizer(null, $stroke);

        //Create rule
        $rule = new Rule();
        $rule->addSymbolizers($symbolizer);

        //Create feature type
        $featuretype = new FeatureTypeStyle();
        $featuretype->addRules($rule);

        $this->assertSame(self::LINE_SYMBOLIZER_XML_PRETTIFIED, $featuretype->applyNamespace(false)->__toString());

    }

}
