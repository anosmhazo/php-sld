<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 1:27 PM
 */

use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\Fill;
use OGC\SLD\SE\Mark;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\WellKnownName;
use PHPUnit\Framework\TestCase;

class SE_MarkTest extends TestCase
{

    const STANDARD_XML = '<Mark><WellKnownName>circle</WellKnownName></Mark>';

    const STANDARD_WITH_FILL_XML =
        '<Mark>'.
        '<WellKnownName>circle</WellKnownName>'.
        '<Fill>'.
        '<CssParameter name="fill">#FF0000</CssParameter>'.
        '</Fill>'.
        '</Mark>';

    const STANDARD_XML_PRETTIFIED =
        "<Mark>\n".
        "\t<WellKnownName>circle</WellKnownName>\n".
        "</Mark>";

    const STANDARD_WITH_FILL_XML_PRETTIFIED =
        "<Mark>\n".
        "\t<WellKnownName>circle</WellKnownName>\n".
        "\t<Fill>\n".
        "\t\t<CssParameter name=\"fill\">#FF0000</CssParameter>\n".
        "\t</Fill>\n".
        "</Mark>";


    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);
        $mark = new Mark($wellKnownName);
        $this->assertSame(self::STANDARD_XML, $mark->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputStandardXMLPrettified(){

        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);
        $mark = new Mark($wellKnownName);
        $this->assertSame(self::STANDARD_XML_PRETTIFIED, $mark->toXML(true));

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputStandardWithFillXML(){

        //Well Known Name
        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#FF0000');
        $fill = new Fill(null, $style1);

        $mark = new Mark($wellKnownName, $fill);
        $this->assertSame(self::STANDARD_WITH_FILL_XML, $mark->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputStandardWithFillXMLPrettified(){

        //Well Known Name
        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#FF0000');
        $fill = new Fill(null, $style1);

        $mark = new Mark($wellKnownName, $fill);
        $this->assertSame(self::STANDARD_WITH_FILL_XML_PRETTIFIED, $mark->toXML(true));

    }

}
