<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 12:10 PM
 */

use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\Fill;
use OGC\SLD\SE\StyleParameter;
use PHPUnit\Framework\TestCase;

class SE_FillTest extends TestCase
{

    const STANDARD_XML = '<Fill></Fill>';
    const CSS_STYLE_XML = '<Fill><CssParameter name="fill">#000080</CssParameter></Fill>';
    const CSS_STYLE_XML_PRETTIFIED =
        "<Fill>\n".
        "\t<CssParameter name=\"fill\">#000080</CssParameter>\n".
        "</Fill>";


    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $fill = new Fill();
        $this->assertSame(self::STANDARD_XML, $fill->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputStyleXML(){

        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#000080');
        $fill = new Fill(null, $style1);
        $this->assertSame(self::CSS_STYLE_XML, $fill->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputStyleXMLPrettified(){

        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#000080');
        $fill = new Fill(null, $style1);
        $this->assertSame(self::CSS_STYLE_XML_PRETTIFIED, $fill->__toString());

    }


}
