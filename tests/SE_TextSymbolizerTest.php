<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/15/2018
 * Time: 2:32 PM
 */

use OGC\SLD\FE\Expression;
use OGC\SLD\SE\AnchorPoint;
use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\Displacement;
use OGC\SLD\SE\Fill;
use OGC\SLD\SE\Font;
use OGC\SLD\SE\Label;
use OGC\SLD\SE\LabelPlacement;
use OGC\SLD\SE\PointPlacement;
use OGC\SLD\SE\Rotation;
use OGC\SLD\SE\TextSymbolizer;
use PHPUnit\Framework\TestCase;

class SE_TextSymbolizerTest extends TestCase
{

    const DATA_SAMPLE1 =
        '<TextSymbolizer>'.
        '<Label>'.
        '<ogc:PropertyName>name</ogc:PropertyName>'.
        '</Label>'.
        '<Font>'.
        '<CssParameter name="font-family">Arial</CssParameter>'.
        '<CssParameter name="font-size">12</CssParameter>'.
        '<CssParameter name="font-style">normal</CssParameter>'.
        '<CssParameter name="font-weight">bold</CssParameter>'.
        '</Font>'.
        '<LabelPlacement>'.
        '<PointPlacement>'.
        '<AnchorPoint>'.
        '<AnchorPointX>0.5</AnchorPointX>'.
        '<AnchorPointY>0</AnchorPointY>'.
        '</AnchorPoint>'.
        '<Displacement>'.
        '<DisplacementX>0</DisplacementX>'.
        '<DisplacementY>25</DisplacementY>'.
        '</Displacement>'.
        '<Rotation>-45</Rotation>'.
        '</PointPlacement>'.
        '</LabelPlacement>'.
        '<Fill>'.
        '<CssParameter name="fill">#990099</CssParameter>'.
        '</Fill>'.
        '</TextSymbolizer>';


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSample1() {

        //Label
        $label_expression = Expression::fromPropertyOnly('name');
        $label = new Label(null, $label_expression);

        //Font
        $styles = [
            CssParameter::fromValue(CssParameter::NAME_FONT_FAMILY, 'Arial'),
            CssParameter::fromValue(CssParameter::NAME_FONT_SIZE, 12),
            CssParameter::fromValue(CssParameter::NAME_FONT_STYLE, 'normal'),
            CssParameter::fromValue(CssParameter::NAME_FONT_WEIGHT, 'bold')
        ];
        $font = new Font(...$styles);

        //Label placement
        $anchorPoint = new AnchorPoint(0.5, 0.0);
        $displacement = new Displacement(0, 25);
        $rotation = Rotation::fromValue(-45);
        $pointPlacement = new PointPlacement($anchorPoint, $displacement, $rotation);
        $labelPlacement = new LabelPlacement($pointPlacement);

        //Fill
        $fill = new Fill(null, CssParameter::fromValue(CssParameter::NAME_FILL_COLOR, '#990099'));

        //Symbolizer
        $symbolizer = new TextSymbolizer(null, $label, $font, $labelPlacement, null, $fill);

        //Assert
        $this->assertSame(self::DATA_SAMPLE1, $symbolizer->toXML());

    }


}
