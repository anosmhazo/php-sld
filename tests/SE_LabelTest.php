<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 1:48 PM
 */

use OGC\SLD\FE\Expression;
use OGC\SLD\SE\Label;
use PHPUnit\Framework\TestCase;

class SE_LabelTest extends TestCase
{

    const DATA_SAMPLE1 = '<Label>'.
        '<ogc:PropertyName>STATE_NAME</ogc:PropertyName>'.
        '(<ogc:PropertyName>STATE_ABBR</ogc:PropertyName>)'.
        '</Label>';


    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $exp1 = Expression::fromPropertyOnly('STATE_NAME');
        $exp2 = Expression::fromPropertyOnly('STATE_ABBR');

        $label = new Label('%s(%s)', $exp1, $exp2);

        $this->assertSame(self::DATA_SAMPLE1, $label->toXML());

    }


}
