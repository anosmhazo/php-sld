<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/3/2018
 * Time: 11:45 AM
 */

use OGC\SLD\FE\Expression;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\SvgParameter;
use PHPUnit\Framework\TestCase;

class SE_SvgParameterTest extends TestCase
{

    const TEST_VALUE_XML = "<SvgParameter name=\"stroke-width\">3</SvgParameter>";

    const TEST_LITERAL_XML =
        "<SvgParameter name=\"stroke-width\">".
        "<ogc:Literal>3</ogc:Literal>".
        "</SvgParameter>";

    const TEST_LITERAL_XML_PRETTIFIED =
        "<SvgParameter name=\"stroke-width\">\n".
        "\t<ogc:Literal>3</ogc:Literal>\n".
        "</SvgParameter>";


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputValueXML(){

        $parameter = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $this->assertSame(self::TEST_VALUE_XML, $parameter->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputLiteralXML(){

        $parameter = SvgParameter::fromExpression(StyleParameter::NAME_STROKE_WIDTH, Expression::fromLiteralOnly(3));
        $this->assertSame(self::TEST_LITERAL_XML, $parameter->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputLiteralXMLPrettified(){

        $parameter = SvgParameter::fromExpression(StyleParameter::NAME_STROKE_WIDTH, Expression::fromLiteralOnly(3));
        $this->assertSame(self::TEST_LITERAL_XML_PRETTIFIED, $parameter->__toString());

    }


}
