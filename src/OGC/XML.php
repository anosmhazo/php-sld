<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 12:08 PM
 */

namespace OGC;


abstract class XML
{

    private $namespacePrefix = 'ogc';
    private $applynsprefix = true;

    abstract public function toXML(bool $prettify = false): string;


    protected function setNamespacePrefix(string $prefix): self {

        $this->namespacePrefix = $prefix;
        return $this;

    }


    protected function generateOpenTag(string $tag, array $attributes = []): string{

        $transformed_attributes = '';
        foreach($attributes as $key => $val)
            $transformed_attributes .= sprintf(' %s="%s"', $key, $val);

        $mark_tag = sprintf(($this->applynsprefix ? '<%s:%s%s>' : '<%2$s%3$s>'), $this->namespacePrefix, $tag, $transformed_attributes);
        return $mark_tag;

    }


    protected function generateOpenSelfClosingTag(string $tag, array $attributes = []): string{

        $transformed_attributes = '';
        foreach($attributes as $key => $val)
            $transformed_attributes .= sprintf(' %s="%s"', $key, $val);

        $mark_tag = sprintf(($this->applynsprefix ? '<%s:%s%s />' : '<%2$s%3$s />'), $this->namespacePrefix, $tag, $transformed_attributes);
        return $mark_tag;

    }


    protected function generateCloseTag(string $tag){
        return sprintf(($this->applynsprefix ? '</%s:%s>' : '</%2$s>'), $this->namespacePrefix, $tag);
    }


    protected function generateXMLVersionXML(): string{
        return '<?xml version="1.0" encoding="UTF-8"?>';
    }


    public function applyNamespace(bool $apply): self{
        $this->applynsprefix = $apply;
        return $this;
    }

}