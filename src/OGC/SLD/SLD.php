<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 2:51 PM
 */

namespace OGC\SLD;


use OGC\XML;

class SLD extends XML
{

    const NAMESPACE_PREFIX = 'sld';

    protected function __construct()
    {

        $this->setNamespacePrefix(self::NAMESPACE_PREFIX);
        $this->applyNamespace(false);

    }

    public function toXML(bool $prettify = false): string
    {
        return '';
    }

}