<?php
/**
 * This OGC Filter Encoding Standard defines NilComparisonOperator as an operator that tests property to see if it exists
 * in the resource that is being evaluated. This corresponds to checking if the property exists in the real world or not
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 11:23 AM
 */

namespace OGC\SLD\FE;


class NullComparisonOperator extends ComparisonOperator
{

    //Comparison names
    const TAG_NIL = 'PropertyIsNull';

    private $expression;


    public function __construct(Expression $expression)
    {

        $this->expression = $expression;

    }


    public function toXML(bool $prettify = false): string
    {

        $xml = sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_NIL),
            preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
            $this->generateCloseTag(self::TAG_NIL));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}