<?php
/**
 * This OGC Filter Encoding Standard defines LikeComparisonOperator as element is intended to encode a character string
 * comparison operator with pattern matching. A combination of regular characters, the wildCard character, the singleChar
 * character, and the escapeChar character define the pattern. The wildCard character matches zero or more characters.
 * The singleChar character matches exactly one character. The escapeChar character is used to escape the meaning of the
 * wildCard, singleChar and escapeChar itself.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 11:23 AM
 */

namespace OGC\SLD\FE;


class LikeComparisonOperator extends ComparisonOperator
{

    //Comparison names
    const TAG_LIKE = 'PropertyIsLike';

    //Match names
    const DEFAULT_WIDCARD = '*';
    const DEFAULT_SINGLE_CHAR = '?';
    const DEFAULT_ESCAPE_CHAR = '\\';

    //Atrribute names
    const ATTR_WILDCARD = 'wildCard';
    const ATTR_SINGLE_CHAR = 'singleChar';
    const ATTR_ESCAPE_CHAR = 'escapeChar';

    private $expression;
    private $wildCard;
    private $singleChar;
    private $escapeChar;


    public function __construct(Expression $expression,
                                string $wildCard = self::DEFAULT_WIDCARD,
                                string $singleChar = self::DEFAULT_SINGLE_CHAR,
                                string $escapeChar = self::DEFAULT_ESCAPE_CHAR) {

        $this->expression = $expression;
        $this->wildCard = $wildCard;
        $this->singleChar = $singleChar;
        $this->escapeChar = $escapeChar;

    }


    public function toXML(bool $prettify = false): string
    {

        if(!$this->expression->hasPropertyAndLiteralOnly() && !$this->expression->hasFunctionAndLiteralOnly())
            throw new \Exception('Expression must have property name or function and literal only defined.');

        $attributes = [
            self::ATTR_WILDCARD => $this->wildCard,
            self::ATTR_ESCAPE_CHAR => $this->escapeChar,
            self::ATTR_SINGLE_CHAR => $this->singleChar
        ];

        $xml = sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_LIKE, $attributes),
            preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
            $this->generateCloseTag(self::TAG_LIKE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}