<?php
/**
 * This OGC Filter Encoding Standard defines BetweenComparisonOperator as a compact way of encoding a range check.
 * The lower and upper boundary values are inclusive.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 11:23 AM
 */

namespace OGC\SLD\FE;


class BetweenComparisonOperator extends ComparisonOperator
{

    const TAG_BETWEEN = 'PropertyIsBetween';

    private $expression;
    private $lowerBoundary;
    private $upperBoundary;


    public function __construct(Expression $expression, Expression $lowerBoundary, Expression $upperBoundary)
    {

        $this->expression = $expression;
        $this->lowerBoundary = $lowerBoundary;
        $this->upperBoundary = $upperBoundary;

    }


    public function toXML(bool $prettify = false): string
    {

        if(!$this->expression instanceof Expression)
            throw new \Exception('Invalid expression.');

        $xml = sprintf(($prettify) ? "%s%s%s%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_BETWEEN),
            preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
            preg_replace("/\n/", "\n\t", $this->lowerBoundary->toXML($prettify)),
            preg_replace("/\n/", "\n\t", $this->upperBoundary->toXML($prettify)),
            $this->generateCloseTag(self::TAG_BETWEEN));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}