<?php
/**
 * The Filter and ElseFilter elements of a Rule allow the selection of features in rules to be
 * controlled by attribute conditions.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 9:20 AM
 */

namespace OGC\SLD\FE;


use OGC\XML;

class Filter extends XML
{

    const TAG_FILTER = 'Filter';
    const TAG_ELSE_FILTER = 'ElseFilter';

    private $operator;


    public function __construct(?FilterOperator $operator = null)
    {
        $this->operator = $operator;
    }


    public function isElseFilter(): bool{
        return $this->operator == null;
    }


    public function toXML(bool $prettify = false): string
    {

        if($this->isElseFilter()) {

            //ElseFilter
            $xml = $this->generateOpenSelfClosingTag(self::TAG_ELSE_FILTER);

        }else{

            //Filter
            $xml = sprintf(($prettify) ? "%s\n\t%s\n%s" : '%s%s%s',
                $this->generateOpenTag(self::TAG_FILTER),
                preg_replace("/\n/", "\n\t", $this->operator->toXML($prettify)),
                $this->generateCloseTag(self::TAG_FILTER));

        }

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}