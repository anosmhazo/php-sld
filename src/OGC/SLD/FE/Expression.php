<?php
/**
 * An expression is a combination of one or more symbols that form part of a predicate. Defined in OGC 09-026r2 standard
 * for filter encoding on page 15.
 *
 * An expression can be formed using the following XML elements:
 * • fes:ValueReference
 * • fes:Literal
 * • fes:Function
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 11:40 AM
 */

namespace OGC\SLD\FE;


use OGC\GML\GeometricPrimitive;
use OGC\XML;

class Expression extends XML
{


    const TAG_PROPERTY_NAME = 'PropertyName';
    const TAG_VALUE_REFERENCE = 'ValueReference';
    const TAG_LITERAL = 'Literal';
    const TAG_FUNCTION = 'Function';

    /**
     * The ogc:PropertyName element can used to specify the name of any property of an object whose value shall
     * be tested by a predicate in a filter expression.
     * @var string
     */
    private $propertyName;

    /**
     * The fes:ValueReference element can used to specify the name of any property of an object whose value shall
     * be tested by a predicate in a filter expression.
     * @var string
     */
    private $valueReference;

    /**
     * A literal value is any part of a statement or expression which should be used as
     * provided
     * @var
     */
    private $literal;

    /**
     * A function is a
     * named procedure that performs a distinct computation. A function can accept zero or more arguments as
     * input and generates a single result.
     * @var
     */
    private $function;


    private function __construct() {}


    /**
     * Create expression from property name and literal
     * @param string $property
     * @param $literal
     * @return Expression
     * @throws \Exception
     */
    public static function fromPropertyAndLiteral(string $property, $literal): self{

        if($literal == null) throw new \Exception('Literal undefined.');

        $me = new self();
        $me->propertyName = $property;
        $me->literal = $literal;

        return $me;

    }


    /**
     * Create expression from literal
     * @param $literal
     * @return Expression
     * @throws \Exception
     */
    public static function fromLiteralOnly($literal): self{

        if($literal == null) throw new \Exception('Literal undefined.');

        $me = new self();
        $me->literal = $literal;

        return $me;

    }


    /**
     * Create expression from property
     * @param string $property
     * @return Expression
     */
    public static function fromPropertyOnly(string $property): self{

        $me = new self();
        $me->propertyName = $property;

        return $me;

    }


    /**
     * Generate xml representation of object
     * @param bool $prettify
     * @return string
     */
    public function toXML(bool $prettify = false): string
    {
        $xml = '';

        //Property name is set
        if($this->propertyName != null){

            $xml .= sprintf(($prettify) ? "\n%s%s%s" : "%s%s%s",
                    $this->generateOpenTag(self::TAG_PROPERTY_NAME),
                    $this->propertyName,
                    $this->generateCloseTag(self::TAG_PROPERTY_NAME)
                );

        }

        //Value reference is set
        if($this->valueReference != null){

            $xml .= sprintf(($prettify) ? "\n%s%s%s": "%s%s%s",
                    $this->generateOpenTag(self::TAG_VALUE_REFERENCE),
                    $this->valueReference,
                    $this->generateCloseTag(self::TAG_VALUE_REFERENCE)
                );

        }

        //Literal is set
        if($this->literal instanceof GeometricPrimitive) {

            $xml .= sprintf(($prettify) ? "\n%s\n\t%s\n%s": "%s%s%s",
                $this->generateOpenTag(self::TAG_LITERAL),
                preg_replace("/\n/", "\n\t", $this->literal->toXML($prettify)),
                $this->generateCloseTag(self::TAG_LITERAL)
            );

        }else if(is_string($this->literal) || is_int($this->literal) || is_float($this->literal) || is_double($this->literal)){

            $literal = preg_replace("[']", "", var_export($this->literal, true));

            $xml .= sprintf(($prettify) ? "\n%s%s%s": "%s%s%s",
                    $this->generateOpenTag(self::TAG_LITERAL),
                    $literal,
                    $this->generateCloseTag(self::TAG_LITERAL)
                );

        }

        return $xml;

    }


    /**
     * Return XML representation of expression object
     * @return string
     */
    public function __toString()
    {
        return $this->toXML(true);
    }


    public function hasPropertyName(): bool{
        return !!$this->propertyName;
    }


    public function hasLiteral(): bool{
        return !!$this->literal;
    }


    public function hasFunction(): bool{
        return !!$this->function;
    }


    public function hasValueReference(): bool{
        return !!$this->valueReference;
    }


    public function hasPropertyAndLiteralOnly(): bool{
        return $this->hasPropertyName() && $this->hasLiteral() && !$this->function && !$this->valueReference;
    }


    public function hasFunctionAndLiteralOnly(): bool{
        return !$this->propertyName && $this->hasLiteral() && $this->hasFunction() && !$this->valueReference;
    }


    public function hasPropertyOnly(): bool{
        return $this->hasPropertyName() && !$this->literal && !$this->function && !$this->valueReference;
    }


    public function hasLiteralOnly(): bool{
        return $this->hasPropertyName() && $this->hasLiteral() && !$this->function && !$this->valueReference;
    }


}