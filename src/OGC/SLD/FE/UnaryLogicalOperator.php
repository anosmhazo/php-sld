<?php
/**
 * Generates XML encoding of logical NOT operator according to OGC Filter Encoding standard 09-026r1
 * User: Anos Kuda Mhazo
 * Date: 12/18/2017
 * Time: 10:13 AM
 */

namespace OGC\SLD\FE;


class UnaryLogicalOperator extends LogicalOperator
{

    const TAG_NOT = 'Not';

    private $operator;


    public function __construct(FilterOperator $operator)
    {
        $this->operator = $operator;
    }


    public function toXML(bool $prettify = false): string
    {

        $xml = sprintf(($prettify) ? "%s\n\t%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_NOT),
            preg_replace("/\n/", "\n\t", $this->operator->toXML($prettify)),
            $this->generateCloseTag(self::TAG_NOT));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}