<?php
/**
 * Encapsulates a layer defined in the style itself
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 11:55 AM
 */

namespace OGC\SLD;


use OGC\SLD\SE\RuleDescription;

class UserLayer extends Layer
{

    const TAG_USER_STYLE = 'UserLayer';

    public $inlineFeature;

    /**
     * @var UserStyle[]
     */
    public $styles = [];


    public function __construct(?string $name = null, ?RuleDescription $description = null)
    {
        parent::__construct();
        $this->setName($name)->setDescription($description);
    }


    public function addStyles(Style ...$styles): self {

        $this->styles = array_merge($this->styles, $styles);
        return $this;

    }


    public function removeStyles(int $index, int $len = -1): self{

        $len = ($len < 0) ? count($this->styles) : $len;
        array_splice($this->styles, $index, $len);
        return $this;

    }


    private function stylesToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->styles, function($carry, $style) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $style->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $styles = '';
        $newline = $prettify ? "\n" : "";
        $parent_xml = $this->generateParentTags($prettify);

        if($this->styles)
            $styles = $newline.$this->stylesToXml($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_USER_STYLE),
            preg_replace("/\n/", "\n\t", $parent_xml),
            preg_replace("/\n/", "\n\t", $styles),
            $this->generateCloseTag(self::TAG_USER_STYLE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}