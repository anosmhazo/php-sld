<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 11:48 AM
 */

namespace OGC\SLD;


use OGC\SLD\SE\FeatureTypeStyle;
use OGC\SLD\SE\RuleDescription;

class UserStyle extends Style
{

    const TAG_NAMED_STYLE = 'UserStyle';

    /**
     * @var null|int
     */
    private $isDefault;

    /**
     * @var FeatureTypeStyle[]
     */
    private $featureTypeStyles = [];

    public function __construct(?string $name = null, ?RuleDescription $description = null)
    {
        $this->setName($name)->setDescription($description);
    }


    public function setDefault(?bool $default): self{

        $this->isDefault = is_bool($default) ? ($default ? 1 : 0) : null;
        return $this;

    }


    public function addFeatureTypeStyles(FeatureTypeStyle ...$featureStyles): self {

        $this->featureTypeStyles = array_merge($this->featureTypeStyles, $featureStyles);
        return $this;

    }


    public function removeFeatureTypeStyles(int $index, int $len = -1): self{

        $len = ($len < 0) ? count($this->featureTypeStyles) : $len;
        array_splice($this->featureTypeStyles, $index, $len);
        return $this;

    }


    private function featureTypeStylesToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->featureTypeStyles, function($carry, $featureTypeStyle) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $featureTypeStyle->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $featureTypeStyles = '';
        $newline = $prettify ? "\n" : "";
        $parent_xml = $this->generateParentTags($prettify);

        if($this->featureTypeStyles)
            $featureTypeStyles = $newline.$this->featureTypeStylesToXml($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_NAMED_STYLE),
            preg_replace("/\n/", "\n\t", $parent_xml),
            preg_replace("/\n/", "\n\t", $featureTypeStyles),
            $this->generateCloseTag(self::TAG_NAMED_STYLE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}