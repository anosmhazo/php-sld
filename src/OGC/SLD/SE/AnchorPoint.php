<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 2:10 PM
 */

namespace OGC\SLD\SE;


class AnchorPoint extends SE
{

    const TAG_ANCHOR_POINT = 'AnchorPoint';
    const TAG_ANCHOR_POINT_X = 'AnchorPointX';
    const TAG_ANCHOR_POINT_Y = 'AnchorPointY';

    private $x;
    private $y;


    public function __construct(float $x, float $y)
    {

        parent::__construct();

        if(abs($x) > 1 || abs($y) > 1)
            throw new \Exception('Invalid anchor point. Values range [0..1]');

        $this->x = $x;
        $this->y = $y;

    }


    public function toXML(bool $prettify = false): string
    {

        $newline = $prettify ? "\n" : "";

        $anchorX = $newline.sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG_ANCHOR_POINT_X),
                $this->x,
                $this->generateCloseTag(self::TAG_ANCHOR_POINT_X));

        $anchorY = $newline.sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG_ANCHOR_POINT_Y),
                $this->y,
                $this->generateCloseTag(self::TAG_ANCHOR_POINT_Y));

        return sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_ANCHOR_POINT),
            preg_replace("/\n/", "\n\t", $anchorX),
            preg_replace("/\n/", "\n\t", $anchorY),
            $this->generateCloseTag(self::TAG_ANCHOR_POINT));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}