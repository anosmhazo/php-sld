<?php
/**
 * Encapsulates the stroking of lines in geometry
 *
 * User: Anos Kuda Mhazo
 * Date: 1/3/2018
 * Time: 10:44 AM
 */

namespace OGC\SLD\SE;


use OGC\XML;

class Stroke extends SE
{

    const TAG_STROKE = 'Stroke';

    private $graphicFill;
    private $graphicStroke;

    /**
     * @var array<StyleParameter>
     */
    private $styles = [];


    protected function __construct()
    {
        parent::__construct();
    }


    public static function fromStyles(StyleParameter ...$styles): self{

        $self = new self();
        $self->styles = $styles;
        return $self;

    }


    private function stylesToXml(bool $prettify){

        //Reduce styles array to XML string
        return array_reduce($this->styles, function($carry, $style) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $style->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $styles = '';
        $graphicFill = '';
        $graphicStroke = '';

        if($this->styles)
            $styles = ($prettify ? "\n" : "").$this->stylesToXml($prettify);
        if($this->graphicFill)
            $graphicFill = ($prettify ? "\n" : "").$this->graphicFill->toXML($prettify);
        if($this->graphicStroke)
            $graphicStroke = ($prettify ? "\n" : "").$this->graphicStroke->toXML($prettify);


        //No namespace needed
        $this->applyNamespace(false);

        $xml = sprintf(($prettify) ? "%s%s%s%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_STROKE),
            preg_replace("/\n/", "\n\t", $styles),
            preg_replace("/\n/", "\n\t", $graphicFill),
            preg_replace("/\n/", "\n\t", $graphicStroke),
            $this->generateCloseTag(self::TAG_STROKE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}