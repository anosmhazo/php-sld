<?php
/**
 * 	The MIME type of the image format. Most standard web image formats are supported. Common MIME types are image/png,
 *  image/jpeg, image/gif, and image/svg+xml
 *
 * User: Anos Kuda Mhazo
 * Date: 1/10/2018
 * Time: 2:12 PM
 */

namespace OGC\SLD\SE;


class Format extends SE
{

    const TAG_FORMAT = 'Format';

    const FORMAT_PNG = 'image/png';
    const FORMAT_JPEG = 'image/jpeg';
    const FORMAT_GIF = 'image/gif';
    const FORMAT_SVG = 'image/svg+xml';

    private $format;

    public function __construct(string $format)
    {

        parent::__construct();
        $this->format = $format;

    }


    public function toXML(bool $prettify = false): string
    {
        return sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_FORMAT),
            $this->format,
            $this->generateCloseTag(self::TAG_FORMAT));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}