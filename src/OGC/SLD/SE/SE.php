<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 2:51 PM
 */

namespace OGC\SLD\SE;


use OGC\XML;

class SE extends XML
{

    const NAMESPACE_PREFIX = 'se';

    protected function __construct()
    {

        $this->setNamespacePrefix(self::NAMESPACE_PREFIX);

        //Disable namespace by default for all children
        $this->applyNamespace(false);

    }

    public function toXML(bool $prettify = false): string
    {
        return '';
    }

}