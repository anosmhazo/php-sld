<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/27/2017
 * Time: 2:39 PM
 */

namespace OGC\SLD\SE;


class PointSymbolizer extends Symbolizer
{

    const TAG_POINT_SYMBOLIZER = 'PointSymbolizer';

    /**
     * @var null|Geometry
     */
    private $geometry;

    /**
     * @var null|Graphic
     */
    private $graphic;


    public function __construct(Graphic $graphic, ?Geometry $geometry = null)
    {

        parent::__construct();
        $this->geometry = $geometry;
        $this->graphic = $graphic;

    }


    public function setGeometry(?Geometry $geometry): self{

        $this->geometry = $geometry;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $graphic_xml = '';
        $geometry_xml = '';
        $newline = $prettify ? "\n" : "";

        if($this->graphic)
            $graphic_xml = $newline.$this->graphic->toXML($prettify);
        if($this->geometry)
            $geometry_xml = $newline.$this->geometry->toXML($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_POINT_SYMBOLIZER),
            preg_replace("/\n/", "\n\t", $graphic_xml),
            preg_replace("/\n/", "\n\t", $geometry_xml),
            $this->generateCloseTag(self::TAG_POINT_SYMBOLIZER));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}