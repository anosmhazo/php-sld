<?php
/**
 * The <Font> element specifes the font to be used for the label. A set of <CssParameter> elements specify the details
 * of the font.
 *
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 11:46 AM
 */

namespace OGC\SLD\SE;


class Font extends SE
{

    const TAG_FONT = 'Font';


    /**
     * @var array|StyleParameter[]
     */
    private $parameters = [];


    public function __construct(StyleParameter ...$parameters)
    {

        parent::__construct();

        $this->parameters = $parameters;

        //Disable namespace by default
        $this->applyNamespace(false);

    }


    public function addStyleParameters(StyleParameter ...$parameters): self{

        $this->parameters = array_merge($this->parameters, $parameters);
        return $this;

    }


    public function removeStyleParameters(int $from, int $len = -1): self{

        $len = ($len < 0) ? count($this->parameters) : $len;
        array_splice($this->parameters, $from, $len);
        return $this;

    }


    private function parametersToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->parameters, function($carry, $parameters) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $parameters->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $parameters_xml = '';
        $newline = $prettify ? "\n" : "";

        //Parameters
        if($this->parameters)
            $parameters_xml = $newline.$this->parametersToXml($prettify);

        return sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_FONT),
            preg_replace("/\n/", "\n\t", $parameters_xml),
            $this->generateCloseTag(self::TAG_FONT));


    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}