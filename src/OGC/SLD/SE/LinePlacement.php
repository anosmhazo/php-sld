<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 2:08 PM
 */

namespace OGC\SLD\SE;


class LinePlacement extends SE
{

    const TAG_LINE_PLACEMENT = 'LinePlacement';

    /**
     * @var null|PerpendicularOffset
     */
    private $perpendicularOffset;


    public function __construct(?PerpendicularOffset $offset = null)
    {

        parent::__construct();

        $this->perpendicularOffset = $offset;

    }


    public function setPerpendicularOffset(?PerpendicularOffset $offset = null): self {

        $this->perpendicularOffset = $offset;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $offset_xml = '';

        $newline = $prettify ? "\n" : "";

        //Offset
        if($this->perpendicularOffset)
            $offset_xml = $newline.$this->perpendicularOffset->toXML($prettify);

        return sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_LINE_PLACEMENT),
            preg_replace("/\n/", "\n\t", $offset_xml),
            $this->generateCloseTag(self::TAG_LINE_PLACEMENT));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}