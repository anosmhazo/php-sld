<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/3/2018
 * Time: 10:50 AM
 */

namespace OGC\SLD\SE;


use OGC\SLD\FE\Expression;

class SvgParameter extends StyleParameter
{

    const TAG = 'SvgParameter';

    /**
     * @var
     */
    private $value;

    /**
     * @var Expression
     */
    private $expression;


    protected function __construct(string $name)
    {
        parent::__construct();
        $this->name = $name;
    }


    /**
     * Creates parameter from primitive value
     * @param string $name
     * @param $value
     * @return SvgParameter
     * @throws \Exception
     */
    public static function fromValue(string $name, $value): self{

        if(is_bool($value) || is_double($value) || is_float($value) || is_string($value) || is_int($value)){

            $self = new self($name);
            $self->value = is_bool($value) ? var_export($value, true) : $value;
            return $self;

        }else{

            throw new \Exception('Invalid value. Only primitive types permitted');

        }


    }


    /**
     * Creates parameter from expression
     * @param string $name
     * @param Expression $expression
     * @return SvgParameter
     */
    public static function fromExpression(string $name, Expression $expression): self{

        $self = new self($name);
        $self->expression = $expression;
        return $self;

    }


    public function toXML(bool $prettify = false): string
    {

        if($this->value !== null){

            $xml = sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG, $this->generateAttributes()),
                $this->value,
                $this->generateCloseTag(self::TAG));

        }else{

            $xml = sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
                $this->generateOpenTag(self::TAG, $this->generateAttributes()),
                preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
                $this->generateCloseTag(self::TAG));

        }

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}