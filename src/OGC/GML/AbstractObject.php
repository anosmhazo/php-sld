<?php
/**
 * GML Abstract Object
 * User: Anos Kuda Mhazo
 * Date: 12/19/2017
 * Time: 12:20 PM
 */

namespace OGC\GML;


use OGC\XML;

class AbstractObject extends XML
{

    const NAMESPACE_PREFIX = 'gml';


    protected function __construct()
    {

        //Set default namespace prefix for all children
        parent::setNamespacePrefix(self::NAMESPACE_PREFIX);

    }


    public function toXML(bool $prettify = false): string
    {
        return '';
    }


}