<?php
/**
 * A boundary of a surface consists of a number of rings. In the normal 2D case, one of these rings is distinguished
 * as being the exterior boundary. In a general manifold this is not always possible, in which case all boundaries
 * shall be listed as interior boundaries, and the exterior will be empty.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/21/2017
 * Time: 8:55 AM
 */

namespace OGC\GML;


class SurfaceExterior extends SurfacePrimitive
{

    const TAG_EXTERIOR = 'exterior';

    private $linearRing;

    public function __construct(RingLinear $linearRing)
    {
        parent::__construct();
        $this->linearRing = $linearRing;

    }


    public function toXML(bool $prettify = false): string
    {

        //Generate attributes
        $attributes = $this->generateAttributes();

        //Generate envelope
        $xml = sprintf(($prettify) ? "%s\n\t%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_EXTERIOR, $attributes),
            preg_replace("/\n/", "\n\t", $this->linearRing->toXML($prettify)),
            $this->generateCloseTag(self::TAG_EXTERIOR));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}