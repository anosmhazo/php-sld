<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/19/2017
 * Time: 3:31 PM
 */

namespace OGC\GML;


class Vector extends Position
{

    const TAG_VECTOR = 'vector';

    private $coordinate;

    public function __construct(Coordinate $coordinate)
    {

        parent::__construct();
        $this->coordinate = $coordinate;

    }


    public function toXML(bool $prettify = false): string
    {

        $attributes = parent::generateAttributes();

        $xml = sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_VECTOR, $attributes),
            $this->coordinate->toXML($prettify),
            $this->generateCloseTag(self::TAG_VECTOR));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}