<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/20/2017
 * Time: 8:51 AM
 */

namespace OGC\GML;


class Envelope extends AbstractGeometricObject
{

    const TAG_ENVELOPE = 'envelope';
    const TAG_LOWER_CORNER = 'lowerCorner';
    const TAG_UPPER_CORNER = 'upperCorner';

    private $lowerCorner;
    private $upperCorner;


    public function __construct(DirectPosition $lowerCorner, DirectPosition $upperCorner)
    {
        parent::__construct();

        $this->lowerCorner = $lowerCorner;
        $this->upperCorner = $upperCorner;

    }


    public function toXML(bool $prettify = false): string
    {

        //Generate attributes
        $attributes = $this->generateAttributes();

        //Get lower corner xml
        $lowerxml = sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_LOWER_CORNER),
            $this->lowerCorner->getCoordinate()->toXML($prettify),
            $this->generateCloseTag(self::TAG_LOWER_CORNER));


        //Get upper corner xml
        $upperxml = sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_UPPER_CORNER),
            $this->upperCorner->getCoordinate()->toXML($prettify),
            $this->generateCloseTag(self::TAG_UPPER_CORNER));

        //Generate envelope
        $xml = sprintf(($prettify) ? "%s\n\t%s\n\t%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_ENVELOPE, $attributes),
            preg_replace("/\n/", "\n\t", $lowerxml),
            preg_replace("/\n/", "\n\t", $upperxml),
            $this->generateCloseTag(self::TAG_ENVELOPE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}