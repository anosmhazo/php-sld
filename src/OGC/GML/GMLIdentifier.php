<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/20/2017
 * Time: 11:36 AM
 */

namespace OGC\GML;


class GMLIdentifier extends AbstractObject
{

    const TAG_IDENTIFIER = 'identifier';
    const ATTR_CODE_SPACE = 'codeSpace';

    private $identifier;
    private $codeSpace;


    public function __construct(string $identifier, ?string $codeSpace = null)
    {
        parent::__construct();

        $this->identifier = $identifier;
        $this->codeSpace = $codeSpace;

    }


    public function toXML(bool $prettify = false): string
    {

        $attributes = [];

        if($this->codeSpace)
            $attributes[self::ATTR_CODE_SPACE] = $this->codeSpace;

        $xml = sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_IDENTIFIER, $attributes),
            $this->identifier,
            $this->generateCloseTag(self::TAG_IDENTIFIER));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}