<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/19/2017
 * Time: 12:29 PM
 */

namespace OGC\GML;


class Position extends AbstractGeometricObject
{


    protected function __construct()
    {
        parent::__construct();
    }


    protected function generateAttributes(): array{

        $attributes = parent::generateAttributes();
        return $attributes;

    }


}