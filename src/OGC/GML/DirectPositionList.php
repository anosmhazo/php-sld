<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/19/2017
 * Time: 12:33 PM
 */

namespace OGC\GML;


class DirectPositionList extends AbstractGeometricObject
{

    const TAG_POSLIST = 'posList';

    const ATTR_COUNT = 'count';
    const ATTR_SRS_DIMENSION = 'srsDimension';

    private $coordinates;
    private $count;

    public function __construct(Coordinate ...$coordinates)
    {

        parent::__construct();
        $this->coordinates = $coordinates;

    }


    public function useCount(bool $use): self{

        $this->count = ($use) ? count($this->coordinates) : null;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $posList = '';
        $dimension = 0;
        $attributes = [];

        foreach($this->coordinates as $coordinate){
            $posList .= (strlen($posList) > 0 ? ' ': '') . $coordinate->toXML($prettify);
            $dimension = $coordinate->getDimensions();
        }

        if($this->count !== null){

            $attributes = [
                self::ATTR_COUNT => $this->count,
                self::ATTR_SRS_DIMENSION => $dimension
            ];

        }

        $xml = sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_POSLIST, $attributes),
            preg_replace("/\n/", "\n\t", $posList),
            $this->generateCloseTag(self::TAG_POSLIST));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}